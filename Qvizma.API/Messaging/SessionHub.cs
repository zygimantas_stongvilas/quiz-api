﻿using Microsoft.AspNetCore.SignalR;
using QVizma.Application.Contracts.Messaging;
using System;
using System.Threading.Tasks;

namespace QVizma.API.Messaging
{
    public class SessionHub : Hub<ISessionClient>
    {
        public async override Task OnConnectedAsync()
        {
            var sessionId = Context.GetHttpContext().Request.Query["sessionId"];
            if (!string.IsNullOrEmpty(sessionId.ToString()))
                await Groups.AddToGroupAsync(Context.ConnectionId, sessionId);
            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            var sessionId = Context.GetHttpContext().Request.Query["sessionId"];
            if (!string.IsNullOrEmpty(sessionId.ToString()))
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, sessionId);
            await base.OnDisconnectedAsync(exception);
        }
    }
}
