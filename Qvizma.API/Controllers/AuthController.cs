﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QVizma.Application.Contracts.Identity;
using QVizma.Application.Models.Authentication;
using System;
using System.Threading.Tasks;

namespace QVizma.API.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IExternalAuthenticationService _authenticationService;

        public AuthController(IExternalAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
        }

        [HttpPost("login")]
        public async Task<ActionResult<ExternalAuthenticationResponse>> LoginAsync(ExternalAuthenticationRequest request)
        {
            return Ok(await _authenticationService.LoginAsync(request));
        }

        [HttpGet("logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            await _authenticationService.LogoutAsync();
            return NoContent();
        }

        [HttpGet("me")]
        [Authorize]
        public async Task<ActionResult<GetUserDataResponse>> GetUserDataAsync()
        {
            return Ok(await _authenticationService.GetUserDataAsync());
        }
    }
}
