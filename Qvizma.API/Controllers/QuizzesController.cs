﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QVizma.Application.Features.Quizzes.Commands.CreateQuiz;
using QVizma.Application.Features.Quizzes.Commands.DeleteQuiz;
using QVizma.Application.Features.Quizzes.Commands.UpdateQuiz;
using QVizma.Application.Features.Quizzes.Queries.GetQuizDetails;
using QVizma.Application.Features.Quizzes.Queries.GetQuizzesCollection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QVizma.API.Controllers
{
    [Authorize]
    [Route("api/quizzes")]
    [ApiController]
    public class QuizzesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public QuizzesController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<QuizCollectionItemVm>>> GetQuizzes()
        {
            var quizzes = await _mediator.Send(new GetQuizzesCollectionQuery());
            return Ok(quizzes);
        }

        [HttpGet("{id}", Name = nameof(GetQuiz))]
        public async Task<ActionResult<QuizDetailsVm>> GetQuiz(int id)
        {
            var quizzes = await _mediator.Send(new GetQuizDetailsQuery { QuizId = id });
            return Ok(quizzes);
        }

        [HttpPost]
        public async Task<ActionResult<CreatedQuizDto>> CreateQuiz([FromBody] CreateQuizCommand command)
        {
            var createdQuiz = await _mediator.Send(command);
            return CreatedAtRoute(nameof(GetQuiz), new { id = createdQuiz.Id }, createdQuiz);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateQuiz(int id, [FromBody] UpdateQuizCommand command)
        {
            await _mediator.Send(new UpdateQuizIncludeIdCommand { QuizId = id, Command = command });
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuiz(int id)
        {
            await _mediator.Send(new DeleteQuizCommand { QuizId = id });
            return NoContent();
        }
    }
}
