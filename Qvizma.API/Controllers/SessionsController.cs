﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using QVizma.API.Messaging;
using QVizma.Application.Contracts.Messaging;
using QVizma.Application.Features.Sessions.Commands.ChangeSessionActivity;
using QVizma.Application.Features.Sessions.Commands.ChangeSessionStatus;
using QVizma.Application.Features.Sessions.Commands.CreateSession;
using QVizma.Application.Features.Sessions.Commands.DeleteSession;
using QVizma.Application.Features.Sessions.Commands.JoinSession;
using QVizma.Application.Features.Sessions.Commands.SubmitAnswer;
using QVizma.Application.Features.Sessions.Commands.UpdateSession;
using QVizma.Application.Features.Sessions.Queries.GetCurrentQuestion;
using QVizma.Application.Features.Sessions.Queries.GetCurrentQuestionSubmissions;
using QVizma.Application.Features.Sessions.Queries.GetSessionAgenda;
using QVizma.Application.Features.Sessions.Queries.GetSessionDetails;
using QVizma.Application.Features.Sessions.Queries.GetSessionLeaderboards;
using QVizma.Application.Features.Sessions.Queries.GetSessionsCollection;
using QVizma.Application.Models.Messaging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QVizma.API.Controllers
{
    [Route("api/sessions")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IHubContext<SessionHub, ISessionClient> _sessionHub;

        public SessionsController(IMediator mediator, IHubContext<SessionHub, ISessionClient> sessionHub)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _sessionHub = sessionHub ?? throw new ArgumentNullException(nameof(sessionHub));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SessionCollectionItemVm>>> GetSessions(
            [FromQuery] bool includePast,
            [FromQuery] bool includeCompleted)
        {
            var sessions = await _mediator.Send(new GetSessionsCollectionQuery { IncludePast = includePast, IncludeCompleted = includeCompleted });
            return Ok(sessions);
        }

        [HttpGet("{code}", Name = nameof(GetSession))]
        public async Task<ActionResult<SessionDetailsVm>> GetSession(string code)
        {
            var session = await _mediator.Send(new GetSessionDetailsQuery { Code = code });
            return Ok(session);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<CreatedSessionDto>> CreateSession([FromBody] CreateSessionCommand command)
        {
            var createdSession = await _mediator.Send(command);
            return CreatedAtRoute(nameof(GetSession), new { code = createdSession.Code}, createdSession);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> UpdateSession(int id, [FromBody] UpdateSessionCommand command)
        {
            await _mediator.Send(new UpdateSessionIncludeIdCommand { Id = id, Command = command });
            return NoContent();
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteSession(int id)
        {
            await _mediator.Send(new DeleteSessionCommand { Id = id });
            return NoContent();
        }

        [HttpPost("{id}/join")]
        public async Task<ActionResult<CreatedParticipantDto>> JoinSession(int id, [FromBody] JoinSessionCommand command)
        {
            var participant = await _mediator.Send(new JoinSessionIncludeIdCommand { SessionId = id, Name = command.Name });
            var message = new ParticipantJoinedMessage { Id = participant.Id, Name = participant.Name };
            _ = _sessionHub.Clients.Group(id.ToString()).ReceiveParticipant(message);
            return Ok(participant);
        }

        [HttpGet("{id}/currentQuestion")]
        public async Task<ActionResult<CurrentQuestionVm>> GetCurrentQuestion(int id)
        {
            var question = await _mediator.Send(new GetCurrentQuestionQuery { SessionId = id });
            return Ok(question);
        }

        [HttpGet("{id}/currentQuestion/submissions")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<SubmissionCollectionItemVm>>> GetCurrentQuestionSubmissions(int id)
        {
            var submissions = await _mediator.Send(new GetCurrentQuestionSubmissionsQuery { SessionId = id });
            return Ok(submissions);
        }

        [HttpPost("{id}/submitAnswer")]
        public async Task<ActionResult<int>> SubmitAnswer(int id, [FromBody] SubmitAnswerCommand command)
        {
            var submissionId = await _mediator.Send(new SubmitAnswerIncludeIdCommand { SessionId = id, ParticipantId = command.ParticipantId, QuestionId = command.QuestionId, AnswerId = command.AnswerId });
            return Ok(submissionId);
        }

        [HttpGet("{id}/leaderboards")]
        public async Task<ActionResult<IEnumerable<LeaderbordsParticipantVm>>> GetSessionLeaderboards(int id)
        {
            var participants = await _mediator.Send(new GetSessionLeaderboardsQuery { SessionId = id });
            return Ok(participants);
        }

        // =================================================================

        [HttpGet("{id}/agenda")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<SessionAgendaQuestionVm>>> GetSessionAgenda(int id)
        {
            var agenda = await _mediator.Send(new GetSessionAgendaQuery { Id = id });
            return Ok(agenda);
        }

        [HttpPost("{id}/changeActivity")]
        [Authorize]
        public async Task<ActionResult<ChangedSessionActivityDto>> ChangeActivity(int id)
        {
            var changedActivity = await _mediator.Send(new ChangeSessionActivityCommand { SessionId = id });
            var message = new StatusChangedMessage { Activity = changedActivity.Activity, CurrentQuestion = changedActivity.CurrentQuestion };
            _ = _sessionHub.Clients.Group(id.ToString()).ReceiveStatus(message);
            return Ok(changedActivity);
        }

        [HttpPost("{id}/changeStatus")]
        [Authorize]
        public async Task<ActionResult<ChangedSessionStatusDto>> ChangeSessionStatus(int id, [FromBody] ChangeSessionStatusCommand command)
        {
            var changedStatus = await _mediator.Send(new ChangeSessionStatusIncludeIdCommand { SessionId = id, Command = command });
            var message = new StatusChangedMessage { Activity = changedStatus.Activity, CurrentQuestion = changedStatus.CurrentQuestion };
            _ = _sessionHub.Clients.Group(id.ToString()).ReceiveStatus(message);
            return Ok(changedStatus);
        }
    }
}
