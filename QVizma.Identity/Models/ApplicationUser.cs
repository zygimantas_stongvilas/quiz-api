﻿using Microsoft.AspNetCore.Identity;

namespace QVizma.Identity.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }
    }
}
