﻿using Google.Apis.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using QVizma.Application.Contracts.Identity;
using QVizma.Application.Exceptions;
using QVizma.Application.Models.Authentication;
using QVizma.Identity.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace QVizma.Identity.Services
{
    public class GoogleAuthenticationService : IExternalAuthenticationService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly JwtSettings _jwtSettings;
        private readonly IConfigurationSection _googleSettings;

        public GoogleAuthenticationService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<JwtSettings> jwtSettings,
            IConfiguration configuration)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _jwtSettings = jwtSettings?.Value ?? throw new ArgumentNullException(nameof(jwtSettings));
            _googleSettings = configuration?.GetSection("GoogleAuthSettings") ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task<ExternalAuthenticationResponse> LoginAsync(ExternalAuthenticationRequest request)
        {
            GoogleJsonWebSignature.Payload externalData;
            try
            {
                externalData = await VerifyGoogleAuthentication(request.IdToken);
            }
            catch (Exception)
            {
                throw new BadRequestException("Invalid External Authentication.");
            }

            var loginInfo = new UserLoginInfo(request.Provider, externalData.Subject, request.Provider);
            var user = await _userManager.FindByLoginAsync(loginInfo.LoginProvider, loginInfo.ProviderKey);

            if (user is null)
            {
                user = new ApplicationUser
                {
                    DisplayName = externalData.Email,
                    Email = externalData.Email,
                    UserName = externalData.Email,
                    EmailConfirmed = true
                };

                await _userManager.CreateAsync(user);
                await _userManager.AddLoginAsync(user, loginInfo);
            }

            var result = await _signInManager.ExternalLoginSignInAsync(loginInfo.LoginProvider, loginInfo.ProviderKey, false);

            if (result.Succeeded)
            {
                var token = await GenerateTokenAsync(user);
                return new ExternalAuthenticationResponse
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    Id = user.Id,
                    Email = user.Email,
                    DisplayName = user.DisplayName,
                };
            }

            // TODO: create custom exception
            throw new Exception();
        }

        public Task LogoutAsync()
        {
            return _signInManager.SignOutAsync();
        }

        public async Task<GetUserDataResponse> GetUserDataAsync()
        {
            var userName = _signInManager.Context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _userManager.FindByNameAsync(userName);
            var userData = new GetUserDataResponse
            {
                Id = user.Id,
                Email = user.Email,
                DisplayName = user.DisplayName
            };
            return userData;
        }

        private async Task<GoogleJsonWebSignature.Payload> VerifyGoogleAuthentication(string idToken)
        {
            var settings = new GoogleJsonWebSignature.ValidationSettings()
            {
                Audience = new[] { _googleSettings.GetSection("ClientId").Value }
            };
            
            var payload = await GoogleJsonWebSignature.ValidateAsync(idToken, settings);
            return payload;
        }

        private async Task<JwtSecurityToken> GenerateTokenAsync(ApplicationUser user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);

            var roleClaims = new List<Claim>();

            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim("roles", roles[i]));
            }

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("uid", user.Id)
            }
            .Union(userClaims)
            .Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtSettings.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }
    }
}
