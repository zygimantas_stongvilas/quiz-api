﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QVizma.Application.Contracts.Persistence;
using QVizma.Identity.Models;
using QVizma.Persistence.Repositories;

namespace QVizma.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<QVizmaDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("QVizmaConnectionString")));
            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IQuizRepository, QuizRepository>();
            services.AddScoped<ISessionRepository, SessionRepository>();
            services.AddScoped<IParticipantRepository, ParticipantRepository>();
            services.AddScoped<ISubmissionRepository, SubmissionRepository>();

            // TODO: should be in QVizma.Identity
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<QVizmaDbContext>()
                .AddDefaultTokenProviders();

            return services;
        }
    }
}
