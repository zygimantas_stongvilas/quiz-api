﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QVizma.Persistence.Migrations
{
    public partial class AddRevealedImageUrlToQuestion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RevealedImageUrl",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RevealedImageUrl",
                table: "Questions");
        }
    }
}
