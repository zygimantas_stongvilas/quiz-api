﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QVizma.Application.Contracts;
using QVizma.Domain.Common;
using QVizma.Domain.Entities;
using QVizma.Identity.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Persistence
{
    public class QVizmaDbContext : IdentityDbContext<ApplicationUser>
    {
        private readonly ILoggedInUserService _loggedInUserService;

        public QVizmaDbContext(DbContextOptions<QVizmaDbContext> options) : base(options)
        {
        }

        public QVizmaDbContext(DbContextOptions<QVizmaDbContext> options, ILoggedInUserService loggedInUserService) : this(options)
        {
            _loggedInUserService = loggedInUserService ?? throw new ArgumentNullException(nameof(loggedInUserService));
        }

        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Submission> Submissions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Session>().HasIndex(session => session.Code).IsUnique();
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(QVizmaDbContext).Assembly);
            SeedTestData(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = DateTimeOffset.Now;
                        entry.Entity.CreatedBy = _loggedInUserService.UserId;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedDate = DateTimeOffset.Now;
                        entry.Entity.LastModifiedBy = _loggedInUserService.UserId;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        private static void SeedTestData(ModelBuilder modelBuilder)
        {
            var firstQuizId = 1;
            var secondQuizId = 2;

            modelBuilder.Entity<Quiz>().HasData(new Quiz
            {
                Id = firstQuizId,
                Title = "1st Quiz",
                Description = "Description of the First Quiz",
                ImageUrl = @"https://source.unsplash.com/random"
            });

            modelBuilder.Entity<Quiz>().HasData(new Quiz
            {
                Id = secondQuizId,
                Title = "2nd Quiz",
                Description = "Description of the Second Quiz",
                ImageUrl = @"https://source.unsplash.com/random"
            });

            var firstQuizFirstQuestionId = 1;
            var firstQuizSecondQuestionId = 2;

            modelBuilder.Entity<Question>().HasData(new Question
            {
                Id = firstQuizFirstQuestionId,
                Text = "Can You Answer The First Question From The First Quiz?",
                ImageUrl = @"https://source.unsplash.com/random",
                Time = 15,
                ShowLeaderboards = true,
                Order = 0,
                QuizId = firstQuizId
            });

            modelBuilder.Entity<Question>().HasData(new Question
            {
                Id = firstQuizSecondQuestionId,
                Text = "Can You Answer The Second Question From The First Quiz?",
                ImageUrl = null,
                Time = null,
                ShowLeaderboards = false,
                Order = 1,
                QuizId = firstQuizId
            });

            var secondQuizFirstQuestionId = 3;

            modelBuilder.Entity<Question>().HasData(new Question
            {
                Id = secondQuizFirstQuestionId,
                Text = "Can You Answer The First Question From The Second Quiz?",
                ImageUrl = @"https://source.unsplash.com/random",
                Time = 5,
                ShowLeaderboards = true,
                Order = 0,
                QuizId = secondQuizId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 1,
                Text = "First Answer",
                IsCorrect = true,
                Order = 0,
                QuestionId = firstQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 2,
                Text = "Second Answer",
                IsCorrect = false,
                Order = 1,
                QuestionId = firstQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 3,
                Text = "Third Answer",
                IsCorrect = false,
                Order = 2,
                QuestionId = firstQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 4,
                Text = "Fourth Answer",
                IsCorrect = false,
                Order = 3,
                QuestionId = firstQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 5,
                Text = "Fifth Answer",
                IsCorrect = false,
                Order = 4,
                QuestionId = firstQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 6,
                Text = "Sixth Answer",
                IsCorrect = false,
                Order = 5,
                QuestionId = firstQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 7,
                Text = "First Answer",
                IsCorrect = true,
                Order = 0,
                QuestionId = firstQuizSecondQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 8,
                Text = "Second Answer",
                IsCorrect = false,
                Order = 1,
                QuestionId = firstQuizSecondQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 9,
                Text = "Yes",
                IsCorrect = true,
                Order = 0,
                QuestionId = secondQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 10,
                Text = "No",
                IsCorrect = false,
                Order = 1,
                QuestionId = secondQuizFirstQuestionId
            });

            modelBuilder.Entity<Answer>().HasData(new Answer
            {
                Id = 11,
                Text = "Maybe",
                IsCorrect = false,
                Order = 2,
                QuestionId = secondQuizFirstQuestionId
            });

            modelBuilder.Entity<Session>().HasData(new Session
            {
                Id = 1,
                ScheduledTime = DateTimeOffset.Parse("2020-08-01T10:00"),
                IsPrivate = false,
                Code = "S1",
                Activity = ActivityType.WaitingForActivation,
                CurrentQuestion = 0,
                QuizId = firstQuizId
            });

            modelBuilder.Entity<Session>().HasData(new Session
            {
                Id = 2,
                ScheduledTime = DateTimeOffset.Parse("2021-08-02T10:00"),
                IsPrivate = false,
                Code = "S2",
                Activity = ActivityType.WaitingForActivation,
                CurrentQuestion = 0,
                QuizId = firstQuizId
            });

            modelBuilder.Entity<Session>().HasData(new Session
            {
                Id = 3,
                ScheduledTime = DateTimeOffset.Parse("2022-08-03T10:00"),
                IsPrivate = true,
                Code = "S3",
                Activity = ActivityType.WaitingForActivation,
                CurrentQuestion = 0,
                QuizId = firstQuizId
            });
        }
    }
}
