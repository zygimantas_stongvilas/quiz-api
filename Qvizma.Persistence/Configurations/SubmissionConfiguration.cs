﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QVizma.Domain.Entities;

namespace QVizma.Persistence.Configurations
{
    public class SubmissionConfiguration : IEntityTypeConfiguration<Submission>
    {
        public void Configure(EntityTypeBuilder<Submission> builder)
        {
            //builder.HasKey(submission => new { submission.ParticipantId, submission.QuestionId });

            builder.HasOne(submission => submission.Question)
                .WithMany()
                .HasForeignKey(submission => submission.QuestionId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(submission => submission.Answer)
                .WithMany()
                .HasForeignKey(submission => submission.AnswerId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
