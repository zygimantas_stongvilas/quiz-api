﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QVizma.Domain.Entities;

namespace QVizma.Persistence.Configurations
{
    public class SessionConfiguration : IEntityTypeConfiguration<Session>
    {
        public void Configure(EntityTypeBuilder<Session> builder)
        {
            builder.Property(session => session.Activity)
                .HasDefaultValue(ActivityType.WaitingForActivation);

            builder.Property(session => session.Code)
                .IsRequired();
        }
    }
}
