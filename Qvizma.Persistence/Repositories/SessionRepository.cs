﻿using Microsoft.EntityFrameworkCore;
using QVizma.Application.Contracts.Persistence;
using QVizma.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QVizma.Persistence.Repositories
{
    public class SessionRepository : BaseRepository<Session>, ISessionRepository
    {
        public SessionRepository(QVizmaDbContext dbContext) : base(dbContext)
        {
        }

        public Task<Session> GetWithQuizAsync(int id)
        {
            return _dbContext.Sessions
                .Include(session => session.Quiz)
                .FirstOrDefaultAsync(session => session.Id == id);
        }

        public async Task<IReadOnlyList<Session>> GetAllWithConditionsAsync(bool includePast, bool includeCompleted)
        {
            var sessions = _dbContext.Sessions.Where(session => !session.IsPrivate);

            if (!includePast)
            {
                var currentTime = DateTimeOffset.Now;
                sessions = sessions.Where(session => session.ScheduledTime > currentTime);
            }

            if (!includeCompleted)
                sessions = sessions.Where(session =>
                    session.Activity == ActivityType.WaitingForActivation ||
                    session.Activity == ActivityType.JoiningEnabled);

            return await sessions
                .Include(session => session.Quiz)
                .ToListAsync();
        }

        public async Task<bool> SessionContainsParticipantAsync(int sessionId, int participantId)
        {
            var participant = await _dbContext.Participants.FindAsync(participantId);
            return participant is not null && participant.SessionId == sessionId;
        }

        public Task<int> GetNumberOfParticipantsAsync(int sessionId)
        {
            return _dbContext.Participants.CountAsync(participant => participant.SessionId == sessionId);
        }

        public Task<Session> GetByCodeAsync(string code)
        {
            return _dbContext.Sessions
                .FirstOrDefaultAsync(session => session.Code.ToLower() == code.ToLower());
        }

        public Task<Session> GetByCodeWithQuizAsync(string code)
        {
            return _dbContext.Sessions
                .Include(session => session.Quiz)
                .FirstOrDefaultAsync(session => session.Code.ToLower() == code.ToLower());
        }
    }
}
