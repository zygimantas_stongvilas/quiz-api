﻿using Microsoft.EntityFrameworkCore;
using QVizma.Application.Contracts.Persistence;
using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QVizma.Persistence.Repositories
{
    public class QuizRepository : BaseRepository<Quiz>, IQuizRepository
    {
        public QuizRepository(QVizmaDbContext dbContext) : base(dbContext)
        {
        }

        public Task<int> GetNumberOfQuestionsAsync(int quizId)
        {
            return _dbContext.Questions
                .CountAsync(question => question.QuizId == quizId);
        }

        public async Task<Quiz> GetQuizWithQuestionsAndAnswersAsync(int quizId)
        {
            var quizzes = _dbContext.Quizzes
                .Include(quiz => quiz.Questions
                    .OrderBy(question => question.Order))
                .ThenInclude(question => question.Answers
                    .OrderBy(answer => answer.Order));
            return await quizzes.FirstOrDefaultAsync(quiz => quiz.Id == quizId);
        }

        public async Task<bool> QuizContainsQuestionAndAnswer(int quizId, int questionId, int answerId)
        {
            var quiz = await _dbContext.Quizzes
                .Include(quiz => quiz.Questions)
                .ThenInclude(question => question.Answers)
                .FirstOrDefaultAsync(quiz => quiz.Id == quizId);
            return quiz is not null &&
                quiz.Questions.Any(question => question.Id == questionId && question.Answers.Any(answer => answer.Id == answerId));
        }

        public async Task<Quiz> GetQuizWithSessionsAsync(int quizId)
        {
            var quiz = await _dbContext.Quizzes
                .Include(quiz => quiz.Sessions
                    .OrderBy(session => session.ScheduledTime))
                .FirstOrDefaultAsync(quiz => quiz.Id == quizId);
            return quiz;
        }

        public async Task<IReadOnlyList<Quiz>> GetUserCreatedQuizzesWithSessionsAsync(string userId)
        {
            return await _dbContext.Quizzes
                .Where(quiz => quiz.CreatedBy == userId)
                .Include(quiz => quiz.Sessions)
                .ToListAsync();
        }
    }
}
