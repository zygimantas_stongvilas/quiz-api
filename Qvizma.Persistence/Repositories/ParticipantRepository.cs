﻿using Microsoft.EntityFrameworkCore;
using QVizma.Application.Contracts.Persistence;
using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QVizma.Persistence.Repositories
{
    public class ParticipantRepository : BaseRepository<Participant>, IParticipantRepository
    {
        public ParticipantRepository(QVizmaDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IReadOnlyList<Participant>> GetAllSessionParticipantsWithSubmissionsAsync(int sessionId)
        {
            return await _dbContext.Participants
                .Where(participant => participant.SessionId == sessionId)
                .Include(participant => participant.Submissions)
                .ToListAsync();
        }

        public Task<Participant> GetByNameAsync(int sessionId, string name)
        {
            return _dbContext.Participants
                .FirstOrDefaultAsync(participant => participant.SessionId == sessionId && participant.Name.ToLower() == name.ToLower());
        }
    }
}
