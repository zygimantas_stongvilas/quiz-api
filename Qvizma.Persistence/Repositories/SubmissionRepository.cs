﻿using Microsoft.EntityFrameworkCore;
using QVizma.Application.Contracts.Persistence;
using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QVizma.Persistence.Repositories
{
    public class SubmissionRepository : BaseRepository<Submission>, ISubmissionRepository
    {
        public SubmissionRepository(QVizmaDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IReadOnlyList<Submission>> GetSubmissionsWithParticipantsForQuestionAsync(int sessionId, int questionId)
        {
            return await _dbContext.Submissions
                .Include(submission => submission.Participant)
                .Where(submission => submission.Participant.SessionId == sessionId && submission.QuestionId == questionId)
                .ToListAsync();
        }

        public Task<Submission> GetSubmissionByParticipantAndQuestionId(int participantId, int questionId)
        {
            return _dbContext.Submissions
                .FirstOrDefaultAsync(submission => submission.ParticipantId == participantId && submission.QuestionId == questionId);
        }
    }
}
