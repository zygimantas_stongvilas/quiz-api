﻿using QVizma.Domain.Entities;

namespace QVizma.Application.Models.Messaging
{
    public class StatusChangedMessage
    {
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
    }
}
