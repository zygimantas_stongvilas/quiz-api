﻿namespace QVizma.Application.Models.Messaging
{
    public class ParticipantJoinedMessage
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
