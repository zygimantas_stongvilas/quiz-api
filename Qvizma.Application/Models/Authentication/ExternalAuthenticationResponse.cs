﻿namespace QVizma.Application.Models.Authentication
{
    public class ExternalAuthenticationResponse
    {
        public string Token { get; set; }
        public string Id { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
    }
}
