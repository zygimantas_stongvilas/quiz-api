﻿namespace QVizma.Application.Models.Authentication
{
    public class GetUserDataResponse
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
    }
}
