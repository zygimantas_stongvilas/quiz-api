﻿namespace QVizma.Application.Models.Authentication
{
    public class ExternalAuthenticationRequest
    {
        public string Provider { get; set; }
        public string IdToken { get; set; }
    }
}
