﻿using QVizma.Application.Models.Authentication;
using System.Threading.Tasks;

namespace QVizma.Application.Contracts.Identity
{
    public interface IExternalAuthenticationService
    {
        Task<ExternalAuthenticationResponse> LoginAsync(ExternalAuthenticationRequest request);
        Task LogoutAsync();
        Task<GetUserDataResponse> GetUserDataAsync();
    }
}
