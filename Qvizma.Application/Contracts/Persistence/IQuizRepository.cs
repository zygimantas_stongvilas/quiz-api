﻿using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QVizma.Application.Contracts.Persistence
{
    public interface IQuizRepository : IAsyncRepository<Quiz>
    {
        Task<int> GetNumberOfQuestionsAsync(int quizId);
        Task<Quiz> GetQuizWithQuestionsAndAnswersAsync(int quizId);
        Task<bool> QuizContainsQuestionAndAnswer(int quizId, int questionId, int answerId);
        Task<Quiz> GetQuizWithSessionsAsync(int quizId);
        Task<IReadOnlyList<Quiz>> GetUserCreatedQuizzesWithSessionsAsync(string userId);
    }
}
