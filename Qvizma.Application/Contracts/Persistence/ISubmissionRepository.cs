﻿using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QVizma.Application.Contracts.Persistence
{
    public interface ISubmissionRepository : IAsyncRepository<Submission>
    {
        Task<IReadOnlyList<Submission>> GetSubmissionsWithParticipantsForQuestionAsync(int sessionId, int questionId);
        Task<Submission> GetSubmissionByParticipantAndQuestionId(int participantId, int questionId);
    }
}
