﻿using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QVizma.Application.Contracts.Persistence
{
    public interface IParticipantRepository : IAsyncRepository<Participant>
    {
        Task<IReadOnlyList<Participant>> GetAllSessionParticipantsWithSubmissionsAsync(int sessionId);
        Task<Participant> GetByNameAsync(int sessionId, string name);
    }
}
