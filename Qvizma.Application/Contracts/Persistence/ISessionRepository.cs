﻿using QVizma.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QVizma.Application.Contracts.Persistence
{
    public interface ISessionRepository : IAsyncRepository<Session>
    {
        Task<Session> GetWithQuizAsync(int id);
        Task<IReadOnlyList<Session>> GetAllWithConditionsAsync(bool includePast, bool includeCompleted);
        Task<bool> SessionContainsParticipantAsync(int sessionId, int participantId);
        Task<int> GetNumberOfParticipantsAsync(int sessionId);
        Task<Session> GetByCodeAsync(string code);
        Task<Session> GetByCodeWithQuizAsync(string code);
    }
}
