﻿using QVizma.Application.Models.Messaging;
using System.Threading.Tasks;

namespace QVizma.Application.Contracts.Messaging
{
    public interface ISessionClient
    {
        Task ReceiveStatus(StatusChangedMessage message);
        Task ReceiveParticipant(ParticipantJoinedMessage message);
    }
}
