﻿using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizQuestionDto
    {
        public int? Id { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public string RevealedImageUrl { get; set; }
        public int? Time { get; set; }
        public bool ShowLeaderboards { get; set; }
        public int Order { get; set; }
        public IEnumerable<UpdateQuizAnswerDto> Answers { get; set; }
    }
}
