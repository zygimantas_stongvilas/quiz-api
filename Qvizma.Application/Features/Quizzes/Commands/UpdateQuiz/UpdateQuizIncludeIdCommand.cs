﻿using MediatR;

namespace QVizma.Application.Features.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizIncludeIdCommand : IRequest
    {
        public int QuizId { get; set; }
        public UpdateQuizCommand Command { get; set; }
    }
}
