﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizCommandHandler : IRequestHandler<UpdateQuizIncludeIdCommand>
    {
        private readonly IMapper _mapper;
        private readonly IQuizRepository _quizRepository;

        public UpdateQuizCommandHandler(IMapper mapper, IQuizRepository quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<Unit> Handle(UpdateQuizIncludeIdCommand request, CancellationToken cancellationToken)
        {
            if (!await _quizRepository.ExistsAsync(request.QuizId))
                throw new NotFoundException(nameof(Quiz), request.QuizId);

            // TODO: Validate request

            var quizToUpdate = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(request.QuizId);
            _mapper.Map(request.Command, quizToUpdate);
            await _quizRepository.UpdateAsync(quizToUpdate);
            return Unit.Value;
        }
    }
}
