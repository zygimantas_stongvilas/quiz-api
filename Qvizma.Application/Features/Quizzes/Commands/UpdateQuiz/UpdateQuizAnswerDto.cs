﻿namespace QVizma.Application.Features.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizAnswerDto
    {
        public int? Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        public int Order { get; set; }
    }
}
