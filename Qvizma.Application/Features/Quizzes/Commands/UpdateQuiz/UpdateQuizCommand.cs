﻿using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizCommand
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsPrivate { get; set; }
        public IEnumerable<UpdateQuizQuestionDto> Questions { get; set; }
    }
}
