﻿using MediatR;

namespace QVizma.Application.Features.Quizzes.Commands.DeleteQuiz
{
    public class DeleteQuizCommand : IRequest
    {
        public int QuizId { get; set; }
    }
}
