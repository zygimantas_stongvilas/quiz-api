﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Quizzes.Commands.DeleteQuiz
{
    public class DeleteQuizCommandHandler : IRequestHandler<DeleteQuizCommand>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Quiz> _quizRepository;

        public DeleteQuizCommandHandler(IMapper mapper, IAsyncRepository<Quiz> quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<Unit> Handle(DeleteQuizCommand request, CancellationToken cancellationToken)
        {
            if (!await _quizRepository.ExistsAsync(request.QuizId))
                throw new NotFoundException(nameof(Quiz), request.QuizId);

            var quiz = await _quizRepository.GetByIdAsync(request.QuizId);
            await _quizRepository.DeleteAsync(quiz);
            return Unit.Value;
        }
    }
}
