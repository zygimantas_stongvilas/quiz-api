﻿using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Commands.CreateQuiz
{
    public class CreatedQuizDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsPrivate { get; set; }
        public IEnumerable<CreatedQuizQuestionDto> Questions { get; set; }
    }
}
