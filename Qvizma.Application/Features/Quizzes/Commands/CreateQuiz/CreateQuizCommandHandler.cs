﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Quizzes.Commands.CreateQuiz
{
    public class CreateQuizCommandHandler : IRequestHandler<CreateQuizCommand, CreatedQuizDto>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Quiz> _quizRepository;

        public CreateQuizCommandHandler(IMapper mapper, IAsyncRepository<Quiz> quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<CreatedQuizDto> Handle(CreateQuizCommand request, CancellationToken cancellationToken)
        {
            // TODO: Validate request

            var quiz = _mapper.Map<Quiz>(request);
            quiz = await _quizRepository.AddAsync(quiz);
            return _mapper.Map<CreatedQuizDto>(quiz);
        }
    }
}
