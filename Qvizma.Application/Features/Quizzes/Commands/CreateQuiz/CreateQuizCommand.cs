﻿using MediatR;
using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Commands.CreateQuiz
{
    public class CreateQuizCommand : IRequest<CreatedQuizDto>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool IsPrivate { get; set; }
        public IEnumerable<CreateQuizQuestionDto> Questions { get; set; }
    }
}
