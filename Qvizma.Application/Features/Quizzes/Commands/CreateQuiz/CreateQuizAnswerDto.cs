﻿namespace QVizma.Application.Features.Quizzes.Commands.CreateQuiz
{
    public class CreateQuizAnswerDto
    {
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        public int Order { get; set; }
    }
}
