﻿namespace QVizma.Application.Features.Quizzes.Commands.CreateQuiz
{
    public class CreatedQuizAnswerDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        public int Order { get; set; }
    }
}
