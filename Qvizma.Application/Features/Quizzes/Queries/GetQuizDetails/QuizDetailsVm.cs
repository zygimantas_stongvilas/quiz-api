﻿using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizDetails
{
    public class QuizDetailsVm
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public IEnumerable<QuizDetailsQuestionDto> Questions { get; set; }
    }
}
