﻿using MediatR;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizDetails
{
    public class GetQuizDetailsQuery : IRequest<QuizDetailsVm>
    {
        public int QuizId { get; set; }
    }
}
