﻿namespace QVizma.Application.Features.Quizzes.Queries.GetQuizDetails
{
    public class QuizDetailsAnswerDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        public int Order { get; set; }
    }
}
