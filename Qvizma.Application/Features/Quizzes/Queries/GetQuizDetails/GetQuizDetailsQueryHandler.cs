﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizDetails
{
    public class GetQuizDetailsQueryHandler : IRequestHandler<GetQuizDetailsQuery, QuizDetailsVm>
    {
        private readonly IMapper _mapper;
        private readonly IQuizRepository _quizRepository;

        public GetQuizDetailsQueryHandler(IMapper mapper, IQuizRepository quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<QuizDetailsVm> Handle(GetQuizDetailsQuery request, CancellationToken cancellationToken)
        {
            var quiz = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(request.QuizId);
            return quiz is null ? throw new NotFoundException(nameof(Quiz), request.QuizId) : _mapper.Map<QuizDetailsVm>(quiz);
        }
    }
}
