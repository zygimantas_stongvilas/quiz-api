﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts;
using QVizma.Application.Contracts.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizzesCollection
{
    public class GetQuizzesCollectionQueryHandler : IRequestHandler<GetQuizzesCollectionQuery, IEnumerable<QuizCollectionItemVm>>
    {
        private readonly IMapper _mapper;
        private readonly IQuizRepository _quizRepository;
        private readonly ISessionRepository _sessionRepository;
        private readonly ILoggedInUserService _loggedInUserService;

        public GetQuizzesCollectionQueryHandler(
            IMapper mapper,
            IQuizRepository quizRepository,
            ISessionRepository sessionRepository,
            ILoggedInUserService loggedInUserService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _loggedInUserService = loggedInUserService ?? throw new ArgumentNullException(nameof(loggedInUserService));
        }

        public async Task<IEnumerable<QuizCollectionItemVm>> Handle(GetQuizzesCollectionQuery request, CancellationToken cancellationToken)
        {
            var userId = _loggedInUserService.UserId;
            var allQuizzes = (await _quizRepository.GetUserCreatedQuizzesWithSessionsAsync(userId)).OrderBy(quiz => quiz.CreatedDate);
            var quizVms = _mapper.Map<IEnumerable<QuizCollectionItemVm>>(allQuizzes);
            foreach (var quiz in quizVms)
            {
                quiz.QuestionsCount = await _quizRepository.GetNumberOfQuestionsAsync(quiz.Id);
                foreach (var session in quiz.Sessions)
                    session.NumberOfParticipants = await _sessionRepository.GetNumberOfParticipantsAsync(session.Id);
            }
            return quizVms;
        }
    }
}
