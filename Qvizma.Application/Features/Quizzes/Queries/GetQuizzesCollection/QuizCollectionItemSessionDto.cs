﻿using QVizma.Domain.Entities;
using System;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizzesCollection
{
    public class QuizCollectionItemSessionDto
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public bool IsPrivate { get; set; }
        public string Code { get; set; }
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
        public int NumberOfParticipants { get; set; }
    }
}
