﻿using MediatR;
using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizzesCollection
{
    public class GetQuizzesCollectionQuery : IRequest<IEnumerable<QuizCollectionItemVm>>
    {
    }
}
