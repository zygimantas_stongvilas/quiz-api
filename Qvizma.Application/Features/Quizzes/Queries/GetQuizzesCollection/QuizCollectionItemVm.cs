﻿using System.Collections.Generic;

namespace QVizma.Application.Features.Quizzes.Queries.GetQuizzesCollection
{
    public class QuizCollectionItemVm
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int QuestionsCount { get; set; }
        public IEnumerable<QuizCollectionItemSessionDto> Sessions { get; set; }
    }
}
