﻿using MediatR;
using System.Collections.Generic;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionsCollection
{
    public class GetSessionsCollectionQuery : IRequest<IEnumerable<SessionCollectionItemVm>>
    {
        public bool IncludePast { get; set; }
        public bool IncludeCompleted { get; set; }
    }
}
