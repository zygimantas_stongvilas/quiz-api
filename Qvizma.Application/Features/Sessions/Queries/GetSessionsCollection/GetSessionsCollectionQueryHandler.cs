﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionsCollection
{
    public class GetSessionsCollectionQueryHandler : IRequestHandler<GetSessionsCollectionQuery, IEnumerable<SessionCollectionItemVm>>
    {
        private readonly IMapper _mapper;
        private readonly IQuizRepository _quizRepository;
        private readonly ISessionRepository _sessionRepository;

        public GetSessionsCollectionQueryHandler(IMapper mapper, IQuizRepository quizRepository, ISessionRepository sessionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<IEnumerable<SessionCollectionItemVm>> Handle(GetSessionsCollectionQuery request, CancellationToken cancellationToken)
        {
            var sessions = (await _sessionRepository.GetAllWithConditionsAsync(request.IncludePast, request.IncludeCompleted))
                .OrderBy(session => session.ScheduledTime);
            var sessionVms = _mapper.Map<IEnumerable<SessionCollectionItemVm>>(sessions);
            foreach (var session in sessionVms)
                session.Quiz.QuestionsCount = await _quizRepository.GetNumberOfQuestionsAsync(session.Quiz.Id);
            return sessionVms;
        }
    }
}
