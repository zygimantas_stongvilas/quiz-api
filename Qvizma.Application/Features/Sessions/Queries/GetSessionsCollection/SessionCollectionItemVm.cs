﻿using QVizma.Domain.Entities;
using System;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionsCollection
{
    public class SessionCollectionItemVm
    {
        public int Id { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public bool IsPrivate { get; set; }
        public string Code { get; set; }
        public ActivityType Activity { get; set; }
        public string CreatedBy { get; set; }
        public SessionCollectionItemQuizDto Quiz { get; set; }
    }
}
