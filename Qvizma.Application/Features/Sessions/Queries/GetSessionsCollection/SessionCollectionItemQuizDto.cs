﻿namespace QVizma.Application.Features.Sessions.Queries.GetSessionsCollection
{
    public class SessionCollectionItemQuizDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int QuestionsCount { get; set; }
    }
}
