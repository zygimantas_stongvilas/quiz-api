﻿using System.Collections.Generic;

namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestion
{
    public class CurrentQuestionVm
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public string RevealedImageUrl { get; set; }
        public int? Time { get; set; }
        public double? RemainingTime { get; set; }
        public bool ShowLeaderboards { get; set; }
        public int Order { get; set; }
        public int? SubmissionAnswerId { get; set; }
        public IEnumerable<CurrentQuestionAnswerDto> Answers { get; set; }
    }
}
