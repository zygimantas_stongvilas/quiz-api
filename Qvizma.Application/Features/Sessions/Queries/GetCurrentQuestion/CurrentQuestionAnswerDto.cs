﻿namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestion
{
    public class CurrentQuestionAnswerDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
    }
}
