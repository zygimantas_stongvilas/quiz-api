﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestion
{
    public class GetCurrentQuestionQuery : IRequest<CurrentQuestionVm>
    {
        public int SessionId { get; set; }
        public int? ParticipantId { get; set; }
    }
}
