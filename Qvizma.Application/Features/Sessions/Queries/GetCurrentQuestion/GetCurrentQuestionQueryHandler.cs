﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestion
{
    public class GetCurrentQuestionQueryHandler : IRequestHandler<GetCurrentQuestionQuery, CurrentQuestionVm>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;
        private readonly IQuizRepository _quizRepository;

        public GetCurrentQuestionQueryHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository,
            IQuizRepository quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<CurrentQuestionVm> Handle(GetCurrentQuestionQuery request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            var quiz = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(session.QuizId);
            var question = quiz.Questions.FirstOrDefault(question => question.Order == session.CurrentQuestion);

            if (question is null)
                return null;

            var questionVm = _mapper.Map<CurrentQuestionVm>(question);
            questionVm.RemainingTime = CalculateRemainingTime(questionVm, session);
            return questionVm;
        }

        private static double? CalculateRemainingTime(CurrentQuestionVm question, Session session)
        {
            if (session.Activity > ActivityType.QuestionEnabled)
            {
                return 0;
            }

            if (session.Activity == ActivityType.QuestionEnabled)
            {
                if (question.Time.HasValue)
                {
                    var elapsedTime = DateTimeOffset.Now.Subtract(session.LastActivityChangeTime);
                    return Math.Max(TimeSpan.FromSeconds(question.Time.Value).Subtract(elapsedTime).TotalSeconds, 0);
                }
                return null;
            }

            return question.Time;
        }
    }
}
