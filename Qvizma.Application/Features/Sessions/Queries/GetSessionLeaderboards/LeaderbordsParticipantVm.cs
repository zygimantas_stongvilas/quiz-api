﻿namespace QVizma.Application.Features.Sessions.Queries.GetSessionLeaderboards
{
    public class LeaderbordsParticipantVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
    }
}
