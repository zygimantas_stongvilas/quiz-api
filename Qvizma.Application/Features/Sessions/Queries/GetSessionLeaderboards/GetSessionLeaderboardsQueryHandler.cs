﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionLeaderboards
{
    public class GetSessionLeaderboardsQueryHandler : IRequestHandler<GetSessionLeaderboardsQuery, IEnumerable<LeaderbordsParticipantVm>>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly IParticipantRepository _participantRepository;

        public GetSessionLeaderboardsQueryHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository,
            IQuizRepository quizRepository,
            IParticipantRepository participantRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
            _participantRepository = participantRepository ?? throw new ArgumentNullException(nameof(participantRepository));
        }

        public async Task<IEnumerable<LeaderbordsParticipantVm>> Handle(GetSessionLeaderboardsQuery request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            var quiz = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(session.QuizId);
            var participants = (await _participantRepository.GetAllSessionParticipantsWithSubmissionsAsync(request.SessionId))
                .OrderByDescending(participant => participant.CreatedDate);

            List<LeaderbordsParticipantVm> participantVms = new();
            foreach (var participant in participants)
            {
                var participantScore = participant.Submissions.Count(submission =>
                {
                    var question = quiz.Questions.FirstOrDefault(question => question.Id == submission.QuestionId);
                    if (question is not null)
                    {
                        var correctAnswer = question.Answers.FirstOrDefault(answer => answer.IsCorrect);
                        return correctAnswer is not null && correctAnswer.Id == submission.AnswerId;
                    }
                    return false;
                });
                var participantVm = _mapper.Map<LeaderbordsParticipantVm>(participant);
                participantVm.Score = participantScore;
                participantVms.Add(participantVm);
            }
            return participantVms
                .OrderByDescending(participant => participant.Score);
        }
    }
}
