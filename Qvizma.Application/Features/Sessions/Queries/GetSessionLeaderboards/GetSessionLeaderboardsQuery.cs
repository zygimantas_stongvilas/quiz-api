﻿using MediatR;
using System.Collections.Generic;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionLeaderboards
{
    public class GetSessionLeaderboardsQuery : IRequest<IEnumerable<LeaderbordsParticipantVm>>
    {
        public int SessionId { get; set; }
    }
}
