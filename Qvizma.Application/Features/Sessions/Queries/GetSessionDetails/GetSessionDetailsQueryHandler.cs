﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionDetails
{
    public class GetSessionDetailsQueryHandler : IRequestHandler<GetSessionDetailsQuery, SessionDetailsVm>
    {
        private readonly IMapper _mapper;
        private readonly ISessionRepository _sessionRepository;

        public GetSessionDetailsQueryHandler(IMapper mapper, ISessionRepository sessionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<SessionDetailsVm> Handle(GetSessionDetailsQuery request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByCodeWithQuizAsync(request.Code);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.Code);

            return _mapper.Map<SessionDetailsVm>(session);
        }
    }
}
