﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionDetails
{
    public class GetSessionDetailsQuery : IRequest<SessionDetailsVm>
    {
        public string Code { get; set; }
    }
}
