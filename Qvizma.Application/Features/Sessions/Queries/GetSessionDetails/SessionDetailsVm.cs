﻿using QVizma.Domain.Entities;
using System;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionDetails
{
    public class SessionDetailsVm
    {
        public int Id { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public bool IsPrivate { get; set; }
        public string Code { get; set; }
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
        public SessionDetailsQuizDto Quiz { get; set; }
    }
}
