﻿using MediatR;
using System.Collections.Generic;

namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestionSubmissions
{
    public class GetCurrentQuestionSubmissionsQuery : IRequest<IEnumerable<SubmissionCollectionItemVm>>
    {
        public int SessionId { get; set; }
    }
}
