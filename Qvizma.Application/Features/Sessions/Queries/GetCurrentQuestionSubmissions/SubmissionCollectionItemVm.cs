﻿namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestionSubmissions
{
    public class SubmissionCollectionItemVm
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public SubmissionParticipantDto Participant { get; set; }
    }
}
