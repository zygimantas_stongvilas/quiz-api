﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestionSubmissions
{
    public class GetCurrentQuestionSubmissionsHandler : IRequestHandler<GetCurrentQuestionSubmissionsQuery, IEnumerable<SubmissionCollectionItemVm>>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly ISubmissionRepository _submissionRepository;

        public GetCurrentQuestionSubmissionsHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository,
            IQuizRepository quizRepository,
            ISubmissionRepository submissionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
            _submissionRepository = submissionRepository ?? throw new ArgumentNullException(nameof(submissionRepository));
        }

        public async Task<IEnumerable<SubmissionCollectionItemVm>> Handle(GetCurrentQuestionSubmissionsQuery request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            var quiz = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(session.QuizId);
            var question = quiz.Questions.FirstOrDefault(question => question.Order == session.CurrentQuestion);

            var submissions = await _submissionRepository.GetSubmissionsWithParticipantsForQuestionAsync(request.SessionId, question.Id);
            return _mapper.Map<IEnumerable<SubmissionCollectionItemVm>>(submissions);
        }
    }
}
