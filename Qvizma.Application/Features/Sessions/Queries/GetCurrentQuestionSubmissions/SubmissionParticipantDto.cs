﻿namespace QVizma.Application.Features.Sessions.Queries.GetCurrentQuestionSubmissions
{
    public class SubmissionParticipantDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
