﻿using MediatR;
using System.Collections.Generic;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionAgenda
{
    public class GetSessionAgendaQuery : IRequest<IEnumerable<SessionAgendaQuestionVm>>
    {
        public int Id { get; set; }
    }
}
