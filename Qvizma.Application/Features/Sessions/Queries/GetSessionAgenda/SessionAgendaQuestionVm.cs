﻿namespace QVizma.Application.Features.Sessions.Queries.GetSessionAgenda
{
    public class SessionAgendaQuestionVm
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public string RevealedImageUrl { get; set; }
        public bool IsTimed { get; set; }
        public int? Time { get; set; }
        public bool ShowLeaderboards { get; set; }
        public int Order { get; set; }
    }
}
