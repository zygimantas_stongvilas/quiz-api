﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionAgenda
{
    public class GetSessionAgendaQueryHandler : IRequestHandler<GetSessionAgendaQuery, IEnumerable<SessionAgendaQuestionVm>>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;
        private readonly IQuizRepository _quizRepository;

        public GetSessionAgendaQueryHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository,
            IQuizRepository quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<IEnumerable<SessionAgendaQuestionVm>> Handle(GetSessionAgendaQuery request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.Id);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.Id);

            var quiz = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(session.QuizId);
            return _mapper.Map<IEnumerable<SessionAgendaQuestionVm>>(quiz.Questions);
        }
    }
}
