﻿using MediatR;
using QVizma.Domain.Entities;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionStatus
{
    public class GetSessionStatusCommand : IRequest<ActivityType>
    {
        public int SessionId { get; set; }
    }
}
