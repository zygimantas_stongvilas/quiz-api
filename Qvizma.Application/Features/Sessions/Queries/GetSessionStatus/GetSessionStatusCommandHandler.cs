﻿using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Queries.GetSessionStatus
{
    public class GetSessionStatusCommandHandler : IRequestHandler<GetSessionStatusCommand, ActivityType>
    {
        private readonly IAsyncRepository<Session> _sessionRepository;

        public GetSessionStatusCommandHandler(IAsyncRepository<Session> sessionRepository)
        {
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<ActivityType> Handle(GetSessionStatusCommand request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            return session.Activity;
        }
    }
}
