﻿using QVizma.Domain.Entities;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionStatus
{
    public class ChangedSessionStatusDto
    {
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
    }
}
