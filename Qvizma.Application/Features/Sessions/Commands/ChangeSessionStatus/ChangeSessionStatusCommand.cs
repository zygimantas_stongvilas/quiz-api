﻿using QVizma.Domain.Entities;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionStatus
{
    public class ChangeSessionStatusCommand
    {
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
    }
}
