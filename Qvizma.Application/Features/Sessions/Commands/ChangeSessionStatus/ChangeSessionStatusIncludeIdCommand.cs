﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionStatus
{
    public class ChangeSessionStatusIncludeIdCommand : IRequest<ChangedSessionStatusDto>
    {
        public int SessionId { get; set; }
        public ChangeSessionStatusCommand Command { get; set; }
    }
}
