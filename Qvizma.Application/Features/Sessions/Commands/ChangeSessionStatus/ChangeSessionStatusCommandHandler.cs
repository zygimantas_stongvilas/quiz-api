﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionStatus
{
    public class ChangeSessionStatusCommandHandler : IRequestHandler<ChangeSessionStatusIncludeIdCommand, ChangedSessionStatusDto>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;

        public ChangeSessionStatusCommandHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<ChangedSessionStatusDto> Handle(ChangeSessionStatusIncludeIdCommand request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            //if (session.Activity == ActivityType.Finished || session.Activity == ActivityType.Canceled)
            //    throw new BadRequestException("This session is already finished.");

            session.Activity = request.Command.Activity;
            session.CurrentQuestion = request.Command.CurrentQuestion;
            session.LastActivityChangeTime = DateTimeOffset.Now;
            await _sessionRepository.UpdateAsync(session);
            return _mapper.Map<ChangedSessionStatusDto>(session);
        }
    }
}
