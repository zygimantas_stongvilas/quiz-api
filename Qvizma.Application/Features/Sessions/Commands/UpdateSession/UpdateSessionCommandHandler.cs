﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.UpdateSession
{
    public class UpdateSessionCommandHandler : IRequestHandler<UpdateSessionIncludeIdCommand>
    {
        private readonly IMapper _mapper;
        private readonly ISessionRepository _sessionRepository;

        public UpdateSessionCommandHandler(IMapper mapper, ISessionRepository sessionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<Unit> Handle(UpdateSessionIncludeIdCommand request, CancellationToken cancellationToken)
        {
            if (!await _sessionRepository.ExistsAsync(request.Id))
                throw new NotFoundException(nameof(Session), request.Id);

            var sessionByCode = await _sessionRepository.GetByCodeAsync(request.Command.Code);
            if (sessionByCode is not null && sessionByCode.Id != request.Id)
                throw new BadRequestException("Code is already taken.");

            // TODO: Validate request

            var sessionToUpdate = await _sessionRepository.GetByIdAsync(request.Id);
            _mapper.Map(request.Command, sessionToUpdate);
            await _sessionRepository.UpdateAsync(sessionToUpdate);
            return Unit.Value;
        }
    }
}
