﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.UpdateSession
{
    public class UpdateSessionIncludeIdCommand : IRequest
    {
        public int Id { get; set; }
        public UpdateSessionCommand Command { get; set; }
    }
}
