﻿using System;

namespace QVizma.Application.Features.Sessions.Commands.UpdateSession
{
    public class UpdateSessionCommand
    {
        public DateTimeOffset ScheduledTime { get; set; }
        public bool IsPrivate { get; set; }
        public string Code { get; set; }
    }
}
