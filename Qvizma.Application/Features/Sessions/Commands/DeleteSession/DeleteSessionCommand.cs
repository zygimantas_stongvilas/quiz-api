﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.DeleteSession
{
    public class DeleteSessionCommand : IRequest
    {
        public int Id { get; set; }
    }
}
