﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.DeleteSession
{
    public class DeleteSessionCommandHandler : IRequestHandler<DeleteSessionCommand>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;

        public DeleteSessionCommandHandler(IMapper mapper, IAsyncRepository<Session> sessionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<Unit> Handle(DeleteSessionCommand request, CancellationToken cancellationToken)
        {
            if (!await _sessionRepository.ExistsAsync(request.Id))
                throw new NotFoundException(nameof(Session), request.Id);

            var session = await _sessionRepository.GetByIdAsync(request.Id);
            await _sessionRepository.DeleteAsync(session);
            return Unit.Value;
        }
    }
}
