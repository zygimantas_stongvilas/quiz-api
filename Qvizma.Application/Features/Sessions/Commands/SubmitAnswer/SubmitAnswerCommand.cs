﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.SubmitAnswer
{
    public class SubmitAnswerCommand
    {
        public int ParticipantId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
    }
}
