﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.SubmitAnswer
{
    public class SubmitAnswerIncludeIdCommand : IRequest<int>
    {
        public int SessionId { get; set; }
        public int ParticipantId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
    }
}
