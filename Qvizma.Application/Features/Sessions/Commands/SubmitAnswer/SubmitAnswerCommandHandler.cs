﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.SubmitAnswer
{
    public class SubmitAnswerCommandHandler : IRequestHandler<SubmitAnswerIncludeIdCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly ISessionRepository _sessionRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly ISubmissionRepository _submissionRepository;

        public SubmitAnswerCommandHandler(
            IMapper mapper,
            ISessionRepository sessionRepository,
            IQuizRepository quizRepository,
            ISubmissionRepository submissionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
            _submissionRepository = submissionRepository ?? throw new ArgumentNullException(nameof(submissionRepository));
        }

        public async Task<int> Handle(SubmitAnswerIncludeIdCommand request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            if (!await _sessionRepository.SessionContainsParticipantAsync(request.SessionId, request.ParticipantId))
                throw new ForbiddenException($"Participant (id: {request.ParticipantId}) is not part of this session (id: {request.SessionId})!");

            if (!await _quizRepository.QuizContainsQuestionAndAnswer(session.QuizId, request.QuestionId, request.AnswerId))
                throw new BadRequestException($"Quiz does not contain specified question (id: {request.QuestionId}) and answer (id: {request.AnswerId}) pair!");

            var existingSubmission = await _submissionRepository.GetSubmissionByParticipantAndQuestionId(request.ParticipantId, request.QuestionId);
            if (existingSubmission is not null)
            {
                _mapper.Map(request, existingSubmission);
                await _submissionRepository.UpdateAsync(existingSubmission);
                return existingSubmission.Id;
            }

            var submission = _mapper.Map<Submission>(request);
            await _submissionRepository.AddAsync(submission);
            return submission.Id;
        }
    }
}
