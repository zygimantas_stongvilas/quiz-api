﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.JoinSession
{
    public class JoinSessionIncludeIdCommand : IRequest<CreatedParticipantDto>
    {
        public int SessionId { get; set; }
        public string Name { get; set; }
    }
}
