﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.JoinSession
{
    public class JoinSessionCommandHandler : IRequestHandler<JoinSessionIncludeIdCommand, CreatedParticipantDto>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;
        private readonly IParticipantRepository _participantRepository;

        public JoinSessionCommandHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository,
            IParticipantRepository participantRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _participantRepository = participantRepository ?? throw new ArgumentNullException(nameof(participantRepository));
        }

        public async Task<CreatedParticipantDto> Handle(JoinSessionIncludeIdCommand request, CancellationToken cancellationToken)
        {
            if (!await _sessionRepository.ExistsAsync(request.SessionId))
                throw new NotFoundException(nameof(Session), request.SessionId);

            if ((await _participantRepository.GetByNameAsync(request.SessionId, request.Name)) is not null)
                throw new BadRequestException("Username is taken!");

            var participant = _mapper.Map<Participant>(request);
            participant = await _participantRepository.AddAsync(participant);
            return _mapper.Map<CreatedParticipantDto>(participant);
        }
    }
}
