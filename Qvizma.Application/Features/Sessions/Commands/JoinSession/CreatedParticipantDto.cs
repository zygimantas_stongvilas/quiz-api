﻿namespace QVizma.Application.Features.Sessions.Commands.JoinSession
{
    public class CreatedParticipantDto
    {
        public int Id { get; set; }
        public int SessionId { get; set; }
        public string Name { get; set; }
    }
}
