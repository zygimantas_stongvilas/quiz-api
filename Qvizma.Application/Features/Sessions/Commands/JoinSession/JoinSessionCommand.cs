﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.JoinSession
{
    public class JoinSessionCommand : IRequest<int>
    {
        public string Name { get; set; }
    }
}
