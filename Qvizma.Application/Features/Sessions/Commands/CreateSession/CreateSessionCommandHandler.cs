﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.CreateSession
{
    public class CreateSessionCommandHandler : IRequestHandler<CreateSessionCommand, CreatedSessionDto>
    {
        private readonly IMapper _mapper;
        private readonly ISessionRepository _sessionRepository;

        public CreateSessionCommandHandler(IMapper mapper, ISessionRepository sessionRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
        }

        public async Task<CreatedSessionDto> Handle(CreateSessionCommand request, CancellationToken cancellationToken)
        {
            // TODO: Validate request

            if ((await _sessionRepository.GetByCodeAsync(request.Code)) is not null)
                throw new BadRequestException("Code is already taken.");

            var session = _mapper.Map<Session>(request);
            session.LastActivityChangeTime = DateTimeOffset.Now;
            session = await _sessionRepository.AddAsync(session);
            return _mapper.Map<CreatedSessionDto>(session);
        }
    }
}
