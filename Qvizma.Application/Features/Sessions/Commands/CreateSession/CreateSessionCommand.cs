﻿using MediatR;
using System;

namespace QVizma.Application.Features.Sessions.Commands.CreateSession
{
    public class CreateSessionCommand : IRequest<CreatedSessionDto>
    {
        public DateTimeOffset ScheduledTime { get; set; }
        public bool IsPrivate { get; set; }
        public string Code { get; set; }
        public int QuizId { get; set; }
    }
}
