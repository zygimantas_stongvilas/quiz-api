﻿using MediatR;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionActivity
{
    public class ChangeSessionActivityCommand : IRequest<ChangedSessionActivityDto>
    {
        public int SessionId { get; set; }
    }
}
