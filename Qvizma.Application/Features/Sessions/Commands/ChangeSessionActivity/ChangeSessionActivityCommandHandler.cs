﻿using AutoMapper;
using MediatR;
using QVizma.Application.Contracts.Persistence;
using QVizma.Application.Exceptions;
using QVizma.Domain.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionActivity
{
    public class ChangeSessionActivityCommandHandler : IRequestHandler<ChangeSessionActivityCommand, ChangedSessionActivityDto>
    {
        private readonly IMapper _mapper;
        private readonly IAsyncRepository<Session> _sessionRepository;
        private readonly IQuizRepository _quizRepository;

        public ChangeSessionActivityCommandHandler(
            IMapper mapper,
            IAsyncRepository<Session> sessionRepository,
            IQuizRepository quizRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sessionRepository = sessionRepository ?? throw new ArgumentNullException(nameof(sessionRepository));
            _quizRepository = quizRepository ?? throw new ArgumentNullException(nameof(quizRepository));
        }

        public async Task<ChangedSessionActivityDto> Handle(ChangeSessionActivityCommand request, CancellationToken cancellationToken)
        {
            var session = await _sessionRepository.GetByIdAsync(request.SessionId);
            if (session is null)
                throw new NotFoundException(nameof(Session), request.SessionId);

            var quiz = await _quizRepository.GetQuizWithQuestionsAndAnswersAsync(session.QuizId);

            switch (session.Activity)
            {
                case ActivityType.Undefined:
                    session.Activity = ActivityType.WaitingForActivation;
                    break;
                case ActivityType.WaitingForActivation:
                    session.Activity = ActivityType.JoiningEnabled;
                    break;
                case ActivityType.JoiningEnabled:
                    {
                        var newQuestion = quiz.Questions.FirstOrDefault();
                        if (newQuestion is null)
                        {
                            session.Activity = ActivityType.Finished;
                        }
                        else
                        {
                            session.Activity = ActivityType.QuestionEnabled;
                            session.CurrentQuestion = newQuestion.Order;
                        }
                        break;
                    }
                case ActivityType.QuestionEnabled:
                    session.Activity = ActivityType.QuestionDisabled;
                    break;
                case ActivityType.QuestionDisabled:
                    session.Activity = ActivityType.QuestionRevealed;
                    break;
                case ActivityType.QuestionRevealed:
                    {
                        var question = quiz.Questions.FirstOrDefault(question => question.Order == session.CurrentQuestion);
                        if (question.ShowLeaderboards)
                        {
                            session.Activity = ActivityType.ShowLeaderboards;
                        }
                        else
                        {
                            var newQuestion = quiz.Questions.FirstOrDefault(question => question.Order > session.CurrentQuestion);
                            if (newQuestion is null)
                            {
                                session.Activity = ActivityType.Finished;
                            }
                            else
                            {
                                session.Activity = ActivityType.QuestionEnabled;
                                session.CurrentQuestion = newQuestion.Order;
                            }
                        }
                        break;
                    }
                case ActivityType.ShowLeaderboards:
                    {
                        var newQuestion = quiz.Questions.FirstOrDefault(question => question.Order > session.CurrentQuestion);
                        if (newQuestion is null)
                        {
                            session.Activity = ActivityType.Finished;
                        }
                        else
                        {
                            session.Activity = ActivityType.QuestionEnabled;
                            session.CurrentQuestion = newQuestion.Order;
                        }
                    }
                    break;
                case ActivityType.Finished:
                    throw new BadRequestException($"Session (id: {request.SessionId}) is already finished!");
                case ActivityType.Canceled:
                    throw new BadRequestException($"Session (id: {request.SessionId}) is already canceled!");
            }

            session.LastActivityChangeTime = DateTimeOffset.Now;
            await _sessionRepository.UpdateAsync(session);
            return _mapper.Map<ChangedSessionActivityDto>(session);
        }
    }
}
