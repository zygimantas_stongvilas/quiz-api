﻿using QVizma.Domain.Entities;

namespace QVizma.Application.Features.Sessions.Commands.ChangeSessionActivity
{
    public class ChangedSessionActivityDto
    {
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
    }
}
