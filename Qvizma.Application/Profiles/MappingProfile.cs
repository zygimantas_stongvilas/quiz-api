﻿using AutoMapper;
using QVizma.Application.Features.Quizzes.Commands.CreateQuiz;
using QVizma.Application.Features.Quizzes.Commands.UpdateQuiz;
using QVizma.Application.Features.Quizzes.Queries.GetQuizDetails;
using QVizma.Application.Features.Quizzes.Queries.GetQuizzesCollection;
using QVizma.Application.Features.Sessions.Commands.ChangeSessionActivity;
using QVizma.Application.Features.Sessions.Commands.ChangeSessionStatus;
using QVizma.Application.Features.Sessions.Commands.CreateSession;
using QVizma.Application.Features.Sessions.Commands.JoinSession;
using QVizma.Application.Features.Sessions.Commands.SubmitAnswer;
using QVizma.Application.Features.Sessions.Commands.UpdateSession;
using QVizma.Application.Features.Sessions.Queries.GetCurrentQuestion;
using QVizma.Application.Features.Sessions.Queries.GetCurrentQuestionSubmissions;
using QVizma.Application.Features.Sessions.Queries.GetSessionAgenda;
using QVizma.Application.Features.Sessions.Queries.GetSessionDetails;
using QVizma.Application.Features.Sessions.Queries.GetSessionLeaderboards;
using QVizma.Application.Features.Sessions.Queries.GetSessionsCollection;
using QVizma.Application.Profiles.Resolvers;
using QVizma.Domain.Entities;

namespace QVizma.Application.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Quiz, QuizCollectionItemVm>();
            CreateMap<Session, QuizCollectionItemSessionDto>();

            CreateMap<Quiz, QuizDetailsVm>();
            CreateMap<Question, QuizDetailsQuestionDto>()
                .ForMember(dest => dest.IsTimed, opt => opt.MapFrom<IsQuestionTimedResolver>());
            CreateMap<Answer, QuizDetailsAnswerDto>();

            CreateMap<CreateQuizCommand, Quiz>();
            CreateMap<CreateQuizQuestionDto, Question>();
            CreateMap<CreateQuizAnswerDto, Answer>();
            CreateMap<Quiz, CreatedQuizDto>();
            CreateMap<Question, CreatedQuizQuestionDto>();
            CreateMap<Answer, CreatedQuizAnswerDto>();

            CreateMap<UpdateQuizCommand, Quiz>();
            CreateMap<UpdateQuizQuestionDto, Question>();
            CreateMap<UpdateQuizAnswerDto, Answer>();

            CreateMap<Session, SessionCollectionItemVm>();
            CreateMap<Quiz, SessionCollectionItemQuizDto>();

            CreateMap<Session, SessionDetailsVm>();
            CreateMap<Quiz, SessionDetailsQuizDto>();

            CreateMap<CreateSessionCommand, Session>();
            CreateMap<Session, CreatedSessionDto>();

            CreateMap<UpdateSessionCommand, Session>();

            CreateMap<JoinSessionIncludeIdCommand, Participant>();
            CreateMap<Participant, CreatedParticipantDto>();

            CreateMap<Participant, LeaderbordsParticipantVm>();

            CreateMap<SubmitAnswerIncludeIdCommand, Submission>();

            CreateMap<Question, CurrentQuestionVm>();
            CreateMap<Answer, CurrentQuestionAnswerDto>();

            CreateMap<Submission, SubmissionCollectionItemVm>();
            CreateMap<Participant, SubmissionParticipantDto>();

            CreateMap<Question, SessionAgendaQuestionVm>()
                .ForMember(dest => dest.IsTimed, opt => opt.MapFrom<IsAgendaQuestionTimedResolver>());

            CreateMap<Session, ChangedSessionActivityDto>();

            CreateMap<Session, ChangedSessionStatusDto>();
        }
    }
}
