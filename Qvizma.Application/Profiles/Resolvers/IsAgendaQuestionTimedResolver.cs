﻿using AutoMapper;
using QVizma.Application.Features.Sessions.Queries.GetSessionAgenda;
using QVizma.Domain.Entities;

namespace QVizma.Application.Profiles.Resolvers
{
    public class IsAgendaQuestionTimedResolver : IValueResolver<Question, SessionAgendaQuestionVm, bool>
    {
        public bool Resolve(Question source, SessionAgendaQuestionVm destination, bool destMember, ResolutionContext context)
        {
            return source.Time.HasValue;
        }
    }
}
