﻿using AutoMapper;
using QVizma.Application.Features.Quizzes.Queries.GetQuizDetails;
using QVizma.Domain.Entities;

namespace QVizma.Application.Profiles.Resolvers
{
    public class IsQuestionTimedResolver : IValueResolver<Question, QuizDetailsQuestionDto, bool>
    {
        public bool Resolve(Question source, QuizDetailsQuestionDto destination, bool destMember, ResolutionContext context)
        {
            return source.Time.HasValue;
        }
    }
}
