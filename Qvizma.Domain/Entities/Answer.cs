﻿using QVizma.Domain.Common;

namespace QVizma.Domain.Entities
{
    public class Answer : AuditableEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
        public int Order { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
