﻿using QVizma.Domain.Common;
using System.Collections.Generic;

namespace QVizma.Domain.Entities
{
    public class Participant : AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int SessionId { get; set; }
        public Session Session { get; set; }

        public ICollection<Submission> Submissions { get; set; }
    }
}
