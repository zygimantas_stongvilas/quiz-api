﻿using QVizma.Domain.Common;

namespace QVizma.Domain.Entities
{
    public class Submission : AuditableEntity
    {
        public int Id { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }

        public int AnswerId { get; set; }
        public Answer Answer { get; set; }

        public int ParticipantId { get; set; }
        public Participant Participant { get; set; }
    }
}
