﻿using QVizma.Domain.Common;
using System;
using System.Collections.Generic;

namespace QVizma.Domain.Entities
{
    public enum ActivityType
    {
        Undefined,
        WaitingForActivation,
        JoiningEnabled,
        QuestionEnabled,
        QuestionDisabled,
        QuestionRevealed,
        ShowLeaderboards,
        Finished,
        Canceled
    }

    public class Session : AuditableEntity
    {
        public int Id { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public bool IsPrivate { get; set; }
        public string Code { get; set; }
        public ActivityType Activity { get; set; }
        public int CurrentQuestion { get; set; }
        public DateTimeOffset LastActivityChangeTime { get; set; }

        public int QuizId { get; set; }
        public Quiz Quiz { get; set; }

        public ICollection<Participant> Participants { get; set; }
    }
}
