﻿using QVizma.Domain.Common;
using System.Collections.Generic;

namespace QVizma.Domain.Entities
{
    public class Question : AuditableEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public string RevealedImageUrl { get; set; }
        public int? Time { get; set; }
        public bool ShowLeaderboards { get; set; }
        public int Order { get; set; }

        public int QuizId { get; set; }
        public Quiz Quiz { get; set; }

        public ICollection<Answer> Answers { get; set; }
    }
}
