﻿using QVizma.Domain.Common;
using System.Collections.Generic;

namespace QVizma.Domain.Entities
{
    public class Quiz : AuditableEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public ICollection<Question> Questions { get; set; }

        public ICollection<Session> Sessions { get; set; }
    }
}
